import enums.Operation;
import interfaces.IAccountService;
import models.Account;
import models.Statement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import services.AccountService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class AccountServiceTest {

    private static IAccountService service;

    @Test
    void testAccessors() {
        Account account = new Account();
        Assertions.assertEquals(0, account.getBalance());
        Assertions.assertEquals(Collections.emptyList(), account.getStatements());

        Statement statement = new Statement();
        Assertions.assertNotNull(statement);
    }

    @Test
    void testTransaction() {
        // Testing null account to avoid nullPointer
        Account account = service.transaction(null, Operation.DEPOSIT, 10);
        Assertions.assertNull(account);

        //Testing Deposit
        account = new Account(0, new ArrayList<>());
        account = service.transaction(account, Operation.DEPOSIT, 20);
        Assertions.assertTrue(account.getStatements().get(0).isPassed());
        Assertions.assertEquals(20.0, account.getBalance());
        Assertions.assertNotNull(account.getStatements());

        //Testing Withdrawal but no available balance
        service.transaction(account, Operation.WITHDRAWAL, 30);
        Assertions.assertFalse(account.getStatements().get(1).isPassed());
        Assertions.assertEquals(20.0, account.getBalance());
        Assertions.assertEquals(2, account.getStatements().size());

        //Testing Withdrawal
        service.transaction(account, Operation.WITHDRAWAL, 10);
        Assertions.assertEquals(10.0, account.getBalance());
        Assertions.assertEquals(3, account.getStatements().size());

        // Testing negative deposit
        account = service.transaction(account, Operation.DEPOSIT, -10);
        Assertions.assertNull(account);

    }

    @BeforeAll
    public static void init(){
        service = new AccountService();
    }
}
