package interfaces;

import enums.Operation;
import models.Account;

public interface IAccountService {

    Account transaction(Account account, Operation operation, double amount);
}
