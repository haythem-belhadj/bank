package services;

import enums.Operation;
import interfaces.IAccountService;
import models.Account;
import models.Statement;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;

public class AccountService implements IAccountService {

    static final Logger logger = Logger.getLogger(AccountService.class);

    /**
     * Transaction which can be either deposit or withdrawal, with operation param.
     * @param account
     * @param operation
     * @param amount
     * @return
     */
    @Override
    public Account transaction(Account account, Operation operation, double amount) {
        if (amount < 0) {
            logger.error("Amount should be positive");
            return null;
        }
        if (account == null) {
            logger.error("Account could not be null");
            return null;
        }
        Statement statement = new Statement(amount, operation, LocalDateTime.now());
        return updateAccountStatements(account, statement);
    }

    /**
     * Updating account statement with transaction result and updating account balance
     * @param account
     * @param statement
     * @return
     */
    private Account updateAccountStatements(Account account, Statement statement) {
        switch (statement.getTransactionType()) {
            case DEPOSIT:
                account.updateBalance(statement.getAmount());
                statement.setPassed(true);
                break;
            case WITHDRAWAL:
                if (account.getBalance() < statement.getAmount()) {
                    statement.setPassed(false);
                    logger.error("We can't proceed transaction. Please check your balance.");
                    break;
                }
                statement.setPassed(true);
                account.updateBalance(-statement.getAmount());
                break;
            default:
                break;
        }
        account.addStatement(statement);
        return account;
    }
}
