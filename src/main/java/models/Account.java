package models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Account {
    private double balance;
    private List<Statement> statements;

    public Account() {
        this.balance = 0;
        this.statements = new ArrayList<>();
    }

    public void updateBalance(double amount) {
        this.balance+= amount;
    }

    public void addStatement(Statement statement) {
        this.statements.add(statement);
    }

}
