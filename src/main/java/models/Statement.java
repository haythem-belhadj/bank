package models;

import enums.Operation;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class Statement {
    double amount;
    boolean passed;
    Operation transactionType;
    LocalDateTime transactionDate;

    public Statement(double amount, Operation transactionType, LocalDateTime transactionDate) {
        this.amount = amount;
        this.transactionType = transactionType;
        this.transactionDate = transactionDate;
    }
}
